# Python CatDog predictor 

Python rest service using Flask for consume a model created with Tensorflow

For running the proyect just follow the next steps:

1. Activate your virtualenv.
2. Install requeriments
    ```
    pip install -r requirements.txt in your shell.
    ```
3. run app
    ```
    python app.py
    ```
    
for testing, do a POST request to the next URL:
    ```
    localhost:6565/upload
    ```
    send an image as form-data using the parameter name:  **image**

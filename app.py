from flask import Flask, url_for, send_from_directory, request
from werkzeug.utils import secure_filename
from werkzeug.datastructures import  FileStorage
import cv2
import keras
import numpy as np
import logging, os

app = Flask(__name__)

CATEGORIES = ['Cat', 'Dog']


def image(img):
    new_arr = cv2.resize(img, (60, 60))
    new_arr = np.array(new_arr)
    new_arr = new_arr.reshape(-1, 60, 60, 1)
    return new_arr

model = keras.models.load_model('3x3x64-catvsdog.model')


@app.route('/upload', methods = ['POST'])
def api_root():
    if request.method == 'POST' and request.files['image']:
        img = cv2.imdecode(np.fromstring(request.files['image'].read(), np.uint8), cv2.IMREAD_UNCHANGED)
        prediction = model.predict([image(img)])
        return CATEGORIES[prediction.argmax()]
    else:
    	return "Where is the image?"

if __name__ == '__main__':
    app.run(debug=True,port=6565)